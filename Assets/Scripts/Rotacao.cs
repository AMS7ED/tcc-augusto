﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Script responsável por rodar o satélite
public class Rotacao : MonoBehaviour
{
    // Valor da rotação
    public float rotacaoEmY = 0;

    // Função chamada no mesmo tempo da física
    private void FixedUpdate()
    {
        transform.Rotate(0, rotacaoEmY * Mathf.Rad2Deg * Time.fixedDeltaTime, 0); // Visualmente gira o objeto
    }
}
