﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Script responsável por fazer os aros girarem
public class ControleDaSimulacao : MonoBehaviour
{
    Camera camera;
    Transform movingObject = null;

    // Chamada antes de começar
    void Awake()
    {
        camera = Camera.main; // Atribui a esta variável a câmera principal
    }

    // Chamada a cada quadro
    void Update()
    {
        // Controle dos aros de controle
        if (Input.GetMouseButtonDown(0)) // Checa entrada do botão esquerdo do mous
        {
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(ray,out hit,1e15f,1<<LayerMask.NameToLayer("Aros controle")) && Time.timeScale>0) // Checa se o clique alcançou o aro
            {
                movingObject = hit.transform;
                movingObject.rotation = Quaternion.LookRotation(hit.point - movingObject.position); // Roda para a posição desejada
                movingObject.rotation = Quaternion.Euler(0, movingObject.eulerAngles.y, 0); // Zera as componentes que não são importantes
            }
        }
        else if (Input.GetMouseButtonUp(0)) // Caso mouse não seja pressionado
        {
            movingObject = null;
        }
        if (movingObject != null)
        {
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 1e15f, 1 << LayerMask.NameToLayer("Zero Plane")))
            {
                movingObject.rotation = Quaternion.LookRotation(hit.point - movingObject.position);
                movingObject.rotation = Quaternion.Euler(0, movingObject.eulerAngles.y, 0);
            }
        }
    }
}
