﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

// Código para a saída na tela dos valores da simulação
public class Telemetria : MonoBehaviour
{
    [SerializeField]
    GameObject campoMagnetico;
    [SerializeField]
    Text PNR;
    [SerializeField]
    GameObject sol;
    [SerializeField]
    Text SR;
    [SerializeField]
    GameObject direcao;
    [SerializeField]
    Text OBJ;
    [SerializeField]
    GameObject satelite;
    [SerializeField]
    Text ATR,RR,PNM,SM,ATM,RM,RV;
    [HideInInspector]
    public float _PNM, _SM, _ATM, _RM, _RV;

    [SerializeField]
    GameObject botaoDetalhes,statusTelemetria;
    [SerializeField]
    GameObject menu;

    // Função chamada no final de cada quadro
    void LateUpdate()
    {
        // Alteração do texto na tela
        OBJ.text = direcao.transform.eulerAngles.y.ToString("F1")+"°";
        float aux = satelite.transform.eulerAngles.y - direcao.transform.eulerAngles.y;
        if (aux < 0) aux += 360;
        ATR.text = satelite.transform.eulerAngles.y.ToString("F1") + "°(Abs) - " + aux.ToString("F1")+"°(Obj)";
        RR.text = satelite.GetComponent<Rotacao>().rotacaoEmY.ToString("F3") + " rad/s";
        aux = campoMagnetico.transform.eulerAngles.y - satelite.transform.eulerAngles.y;
        if (aux < 0) aux += 360;
        PNR.text = campoMagnetico.transform.eulerAngles.y.ToString("F1") + "°(Abs) - " + aux.ToString("F1") + "°(Rel)";
        aux = sol.transform.eulerAngles.y - satelite.transform.eulerAngles.y;
        if (aux < 0) aux += 360;
        SR.text = sol.transform.eulerAngles.y.ToString("F1") + "°(Abs) - " + aux.ToString("F1") + "°(Rel)";

        PNM.text = _PNM.ToString("F1") + "°(R)";
        SM.text = _SM.ToString("F1") + "°(R)";
        ATM.text = _ATM.ToString("F1") + "°(R)";
        RM.text = _RM.ToString("F3") + " rad/s";
        RV.text = (_RV/(2*Mathf.PI)).ToString("F2") + " rps";
    }

    bool detalhes = false;
    public void botaoControleUI() // Função que ativa ou desativa o controle, chamada pelo botão
    {
        if (detalhes) // Desativa
        {
            botaoDetalhes.GetComponent<UnityEngine.UI.Image>().color = Color.red;
            botaoDetalhes.GetComponentInChildren<Text>().color = Color.white;
            detalhes = false;
            statusTelemetria.SetActive(false);
        }
        else // Ativa
        {
            botaoDetalhes.GetComponent<UnityEngine.UI.Image>().color = Color.green;
            botaoDetalhes.GetComponentInChildren<Text>().color = Color.black;
            detalhes = true;
            statusTelemetria.SetActive(true);
        }
    }

    public void botaoMenu() // Função que ativa ou desativa o menu
    {
        if (menu.activeSelf) Time.timeScale = 1; // Desativando menu
        else // Ativando menu
        {
            Time.timeScale = 0; // Pausa simulação
            Menu.menu.Revert();
        }
        menu.SetActive(!menu.activeSelf);
    }


    // --- Saída para arquivo ---

    string fileName;
    bool recording;
    FileStream fs = null;
    StreamWriter sw;
    float recordStartTime;
    [Header("Gravação")]
    [SerializeField]
    GameObject botaoGravar;
    string dir;

    // Chamada no início da simulação
    private void Start()
    {
        dir = Application.dataPath + "\\Outputs\\";
        Directory.CreateDirectory(dir);
    }

    // Inicia gravação
    void iniciarDataRecord()
    {
        recordStartTime = 0;
        fileName = dir + System.DateTime.Now.ToString("yyyy-MM-dd T’HH-mm-ss") + ".txt";
        fs = null;
        try
        {
            fs = new FileStream(fileName, FileMode.CreateNew,FileAccess.Write);
        }
        finally
        {
            if (fs != null)
            {
                sw = new StreamWriter(fs);
                recording = true;
                botaoGravar.GetComponent<UnityEngine.UI.Image>().color = Color.green;
                botaoGravar.GetComponentInChildren<Text>().color = Color.black;
            }
        }
    }

    // Chamada nun ubtervalo fixo de tempo (o mesmo da física)
    private void FixedUpdate()
    {
        if (recording)
        {
            if (fs != null)
            {
                // Formato saída: Tempo[s] Erro[°] RotSat[rad/s] RotRoda[rps]
                string textToAdd = (Time.time - recordStartTime).ToString("000.000");
                float aux = satelite.transform.eulerAngles.y - direcao.transform.eulerAngles.y;
                if (aux > 180) aux -= 360;
                textToAdd += " " + aux.ToString("E5");
                textToAdd += " " + satelite.GetComponent<Rotacao>().rotacaoEmY.ToString("E5");
                textToAdd += " " + (_RV / (2 * Mathf.PI)).ToString("E5");

                sw.WriteLine(textToAdd);

                //fs.Dispose();
            }
        }
    }

    // Interrompe a gravação
    void pararDataRecord()
    {
        if(fs!=null) fs.Close();
        recording = false;
        botaoGravar.GetComponent<UnityEngine.UI.Image>().color = Color.red;
        botaoGravar.GetComponentInChildren<Text>().color = Color.white;
    }

    // Função chamada pelo botão de gravação
    public void recordButton()
    {
        if (recording) pararDataRecord();
        else iniciarDataRecord();
    }
}
