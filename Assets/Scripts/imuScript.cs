﻿//using NUnit.Framework;
//using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Script do IMU
public class imuScript : MonoBehaviour
{
    [SerializeField]
    GameObject satelite;
    [SerializeField]
    AnimationCurve erroSistemicoPorValorRealGiroscopio;
    [SerializeField]
    AnimationCurve desvioPadraoPorValorRealGiroscopio;

    [SerializeField]
    GameObject campoMagnetico;
    [SerializeField]
    AnimationCurve erroSistemicoPorValorRealMagnetometro;
    [SerializeField]
    AnimationCurve desvioPadraoPorValorRealMagnetometro;

    public float avaliarRotacaoIMU()
    {
        float valorReal = satelite.GetComponent<Rotacao>().rotacaoEmY; // Leitura do valor real
        return valorReal + CustomMath.gaussianRandom(erroSistemicoPorValorRealGiroscopio.Evaluate(valorReal), desvioPadraoPorValorRealGiroscopio.Evaluate(valorReal)); // Adição de erro normal
    }

    public float avaliarCampoMagneticoIMU()
    {
        float valorReal = transform.eulerAngles.y-campoMagnetico.transform.eulerAngles.y;
        float valorMedido= valorReal + CustomMath.gaussianRandom(erroSistemicoPorValorRealMagnetometro.Evaluate(valorReal), desvioPadraoPorValorRealMagnetometro.Evaluate(valorReal));
        // Limita entre 0° e 360°
        while (valorMedido >= 360) valorMedido -= 360;
        while (valorMedido < 0) valorMedido += 360;
        return valorMedido;
    }
}