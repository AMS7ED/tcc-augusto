﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Script do motor
public class MotorScript : MonoBehaviour
{
    [SerializeField]
    float maxSpeed = 6e3f; //RPS
    [SerializeField]
    GameObject roda;
    [HideInInspector]
    public float rotacaoDesejada;

    enum tipo
    {
        simples,completo
    }
    [SerializeField]
    tipo tipoDeMotor = tipo.completo;
    
    [Serializable]
    public struct motorParam
    {
        public float b, // Constante de fricção viscosa do motor (N.m.s)
            R, // Resistência elétrica (ohm)
            K, // Constante de torque do motor (N.m/Amp)
            L; // Indutância (H)
        [HideInInspector]
        public float J; //Momento de inercia da roda (kg.m²)
    }
    [HideInInspector]
    public motorParam paramDoMotor;

    float c1, c2,k1,k2;
    // Chamada antes do primeiro quadro
    private void Start()
    {
        paramDoMotor.J = roda.GetComponent<PropriedadesDoCorpo>().Iyy;
        paramDoMotor.b = PlayerPrefs.GetFloat("b", .2f);
        paramDoMotor.K = PlayerPrefs.GetFloat("K", .02f);
        paramDoMotor.R = PlayerPrefs.GetFloat("R", 1);
        paramDoMotor.L = PlayerPrefs.GetFloat("L", .3f);

        if (tipoDeMotor == tipo.completo)
        {
            calcParamMotorCompleto(paramDoMotor);
        }
        maxSpeed *= 2 * Mathf.PI; // rps → rad/s
    }

    public void calcParamMotorCompleto(motorParam _) // O uso de "_" foi para não ter que escrever "paramDoMotor" o tempo todo, clareando a visualização
    {
        paramDoMotor = _;
        float c;
        c = Mathf.Sqrt(_.b * _.b * _.L * _.L - 2 * _.b * _.J * _.L * _.R + _.J * _.J * _.R * _.R - 4 * _.J * _.K * _.K * _.L);
        c1 = -c / (2 * _.J * _.L) - _.b / (2 * _.J) - _.R / (2 * _.L);
        c2 = c / (2 * _.J * _.L) - _.b / (2 * _.J) - _.R / (2 * _.L);
        k1 = (-_.b * _.L - _.J * _.R + c) / (2 * c);
        k2 = (_.b * _.L + _.J * _.R + c) / (2 * c);
    }

    // Chamada a cada valor fixo de tempo (física)
    private void FixedUpdate()
    {
        if(tipoDeMotor==tipo.simples)roda.GetComponent<Rotacao>().rotacaoEmY= dimanicaDoMotorSimples();
        else if (tipoDeMotor == tipo.completo) roda.GetComponent<Rotacao>().rotacaoEmY = dimanicaDoMotorCompleta();
    }

    // Indutância desprezível, retorna a rootação
    float dimanicaDoMotorSimples()
    {
        float rotacaoAtual = roda.GetComponent<Rotacao>().rotacaoEmY;
        rotacaoAtual =rotacaoDesejada-(rotacaoDesejada-rotacaoAtual) *Mathf.Exp((-Time.fixedDeltaTime) * (paramDoMotor.b * paramDoMotor.R + paramDoMotor.K * paramDoMotor.K) / (paramDoMotor.R * paramDoMotor.J));
        return Mathf.Clamp(rotacaoAtual, -maxSpeed, maxSpeed);
    }

    // Retorna a rotação
    float dimanicaDoMotorCompleta()
    {
        float rotacaoAtual = roda.GetComponent<Rotacao>().rotacaoEmY;
        float aux = Mathf.Clamp(rotacaoDesejada, -maxSpeed, maxSpeed);
        rotacaoAtual = aux - (aux - rotacaoAtual) * (k1 * Mathf.Exp(Time.fixedDeltaTime * c1) + k2 * Mathf.Exp(Time.fixedDeltaTime * c2));
        return Mathf.Clamp(rotacaoAtual, -maxSpeed, maxSpeed);
    }
}
