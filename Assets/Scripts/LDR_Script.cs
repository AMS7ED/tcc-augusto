﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Script do LDR
public class LDR_Script : MonoBehaviour
{
    [SerializeField]
    GameObject sol;
    [SerializeField]
    AnimationCurve erroSistemicoPorValorReal;
    [SerializeField]
    AnimationCurve desvioPadraoPorValorReal;

    public float avaliarLDR()
    {
        float valorReal = Mathf.Abs(Vector3.Angle(sol.transform.forward, transform.forward));
        // Limitando leitura entre 0° e 90°
        if (valorReal > 90) valorReal = 90;
        else if (valorReal <= -90) valorReal = -90;
        return valorReal + CustomMath.gaussianRandom(erroSistemicoPorValorReal.Evaluate(valorReal), desvioPadraoPorValorReal.Evaluate(valorReal)); // Adição de erro normal
    }
}
