﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Classe para controle da física da simulação (física da Unity não é utilizada)
public class ScriptDaFisica : MonoBehaviour
{
    [SerializeField]
    GameObject satelite;
    Rotacao satRot;
    PropriedadesDoCorpo satProp;
    [SerializeField]
    GameObject roda;
    Rotacao rodaRot;
    PropriedadesDoCorpo rodaProp;
    public float deltaRotacaoSatelite = 0;
    public bool aplicarRotacao=false;
    float H;
    [SerializeField]
    Text addRotInput;

    // Chamada antes da Start()
    private void Awake()
    {
        Time.timeScale = 1; // Isto serve para a simulação "despausar" após ser reiniciada pelo menu
    }

    // Chamada antes do primeiro quadro
    private void Start()
    {
        satRot = satelite.GetComponent<Rotacao>();
        satProp = satelite.GetComponent<PropriedadesDoCorpo>();
        rodaRot = roda.GetComponent<Rotacao>();
        rodaProp = roda.GetComponent<PropriedadesDoCorpo>();
        H = satRot.rotacaoEmY * satProp.Iyy + rodaRot.rotacaoEmY * rodaProp.Iyy; // Cálculo do momento angular
    }

    void FixedUpdate()
    {
        satRot.rotacaoEmY = (H - rodaRot.rotacaoEmY * rodaProp.Iyy) / satProp.Iyy; // Cálculo da nova rotação do satélite
        if (aplicarRotacao) // Caso um degrau na rotação do satélite seja inserido pelo usuário
        {
            aplicarRotacao = false;
            satRot.rotacaoEmY += deltaRotacaoSatelite;
            deltaRotacaoSatelite = 0; // Reseta o valor inserido
            H = satRot.rotacaoEmY * satProp.Iyy + rodaRot.rotacaoEmY * rodaProp.Iyy; // Cálculo do novo momento angular
        }
    }

    // Função chamada para adicionar degrau na rotação
    public void addRotField()
    {
        float.TryParse(addRotInput.text.Replace(".", ","), out float a);
        deltaRotacaoSatelite = a;
    }

    // Função chamada para aplicar o degrau
    public void addRotButton()
    {
        aplicarRotacao = true;
        addRotInput.GetComponentInParent<InputField>().text = "";
    }
}

// Classe utilizada para gerar valor gaussiano aleatório
public class CustomMath
{
    static float spare;
    static bool hasSpare = false;
    public static float gaussianRandom(float mean, float stdDev)
    {
        if (hasSpare)
        {
            hasSpare = false;
            return spare * stdDev + mean;
        }
        else
        {
            float u, v, s;
            do
            {
                u = Random.Range(-1f, 1f);
                v = Random.Range(-1f, 1f);
                s = u * u + v * v;
            } while (s >= 1 || s == 0);
            s = Mathf.Sqrt(-2f * Mathf.Log(s) / s);
            spare = v * s;
            hasSpare = true;
            return mean + stdDev * u * s;
        }
    }
}
