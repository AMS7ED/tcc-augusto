﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// Script responsável pelo menu
public class Menu : MonoBehaviour
{
    [SerializeField]
    MotorScript motor;
    [SerializeField]
    PropriedadesDoCorpo sat,roda;
    [SerializeField]
    OBDH obdh;
    float K, b, R, L, J, Iy, Kp, Kd;
    [Header("Input Fields")]
    [SerializeField]
    InputField if_K;
    [SerializeField]
    InputField if_b, if_R, if_L, if_J, if_Iy, if_Kp, if_Kd;
    Text ph_K, ph_b, ph_R, ph_L, ph_J, ph_Iy, ph_Kp, ph_Kd;

    public static Menu menu;

    // Executado antes de todo o resto
    private void Awake()
    {
        ph_K = if_K.placeholder.GetComponent<Text>();
        ph_b = if_b.placeholder.GetComponent<Text>();
        ph_R = if_R.placeholder.GetComponent<Text>();
        ph_L = if_L.placeholder.GetComponent<Text>();
        ph_J = if_J.placeholder.GetComponent<Text>();
        ph_Iy = if_Iy.placeholder.GetComponent<Text>();
        ph_Kp = if_Kp.placeholder.GetComponent<Text>();
        ph_Kd = if_Kd.placeholder.GetComponent<Text>();

        menu = GetComponent<Menu>();

        // Carrega valores da memória
        sat.Iyy = PlayerPrefs.GetFloat("Iy", .0022f);
        roda.Iyy = PlayerPrefs.GetFloat("J", 2e-6f);

    }

    // Reseta a simulação com os novos valores estipulados
    public void Apply()
    {
        float aux;
        if (if_K.text != "")
        {
            float.TryParse(if_K.text.Replace(".", ","), out aux);
            if (aux > 0) PlayerPrefs.SetFloat("K",aux);
        }
        if (if_b.text != "")
        {
            float.TryParse(if_b.text.Replace(".", ","), out aux);
            if (aux > 0) PlayerPrefs.SetFloat("b", aux);
        }
        if (if_R.text != "")
        {
            float.TryParse(if_R.text.Replace(".", ","), out aux);
            if (aux > 0) PlayerPrefs.SetFloat("R", aux);
        }
        if (if_L.text != "")
        {
            float.TryParse(if_L.text.Replace(".", ","), out aux);
            if (aux > 0) PlayerPrefs.SetFloat("L", aux);
        }
        if (if_J.text != "")
        {
            float.TryParse(if_J.text.Replace(".", ","), out aux);
            if (aux > 0) PlayerPrefs.SetFloat("J", aux);
        }
        if (if_Iy.text != "")
        {
            float.TryParse(if_Iy.text.Replace(".", ","), out aux);
            if (aux > 0) PlayerPrefs.SetFloat("Iy", aux);
        }
        if (if_Kp.text != "")
        {
            float.TryParse(if_Kp.text.Replace(".", ","), out aux);
            if (aux > 0) PlayerPrefs.SetFloat("Kp", aux);
        }
        if (if_Kd.text != "")
        {
            float.TryParse(if_Kd.text.Replace(".", ","), out aux);
            if (aux > 0) PlayerPrefs.SetFloat("Kd", aux);
        }

        SceneManager.LoadScene(SceneManager.GetActiveScene().name); // Reseta a simulação
    }

    // Retorna os valores anteriores à edição
    public void Revert()
    {
        K = motor.paramDoMotor.K;
        if_K.text = "";
        ph_K.text = K.ToString();
        b = motor.paramDoMotor.b;
        if_b.text = "";
        ph_b.text = b.ToString();
        R = motor.paramDoMotor.R;
        if_R.text = "";
        ph_R.text = R.ToString();
        L = motor.paramDoMotor.L;
        if_L.text = "";
        ph_L.text = L.ToString();
        J = motor.paramDoMotor.J;
        if_J.text = "";
        ph_J.text = J.ToString();
        Iy = sat.Iyy;
        if_Iy.text = "";
        ph_Iy.text = Iy.ToString();
        Kp = obdh.Kp;
        if_Kp.text = "";
        ph_Kp.text = Kp.ToString();
        Kd = obdh.Kd;
        if_Kd.text = "";
        ph_Kd.text = Kd.ToString();
    }
}
