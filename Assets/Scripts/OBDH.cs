﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.UI;

// Script do computador de bordo
public class OBDH : MonoBehaviour
{
    // Declaração de variáveis
    [SerializeField]
    PropriedadesDoCorpo propSat;
    [SerializeField]
    GameObject LDR_Frente;
    [SerializeField]
    GameObject LDR_Esquerda;
    [SerializeField]
    GameObject LDR_Direita;
    [SerializeField]
    GameObject LDR_Verso;
    [SerializeField]
    GameObject IMU;
    [SerializeField]
    GameObject motor;
    [SerializeField]
    GameObject roda;
    PropriedadesDoCorpo propRoda;
    [SerializeField]
    GameObject direcao;
    [SerializeField]
    float toleranciaRotacao = 0;
    [SerializeField]
    float toleranciaAngulo = 0;
    [SerializeField]
    bool controleAtivado = false;
    [SerializeField]
    Telemetria telemetria;
    [SerializeField]
    GameObject botaoControle;
    [HideInInspector]
    public float Kp=1,Kd=1;
    float rotEquilibrio;
    [SerializeField]
    float rotacaoLimiteControle;

    struct DOF
    {
        public float angPosY;
        public float angVelY;
    }
    DOF atitude; // Representação da posição angular e velocidade angular em torno do eixo vertical

    // Executado antes do primeiro quadro
    private void Start()
    {
        propRoda = roda.GetComponent<PropriedadesDoCorpo>();
        // Recupera valores salvos
        Kp = PlayerPrefs.GetFloat("Kp", 1);
        Kd = PlayerPrefs.GetFloat("Kd", 1);

    }

    // Executado em períodos constantes (o mesmo da física)
    private void FixedUpdate()
    {
        determinacaoAtitude();
        // Modifica as variáveis no script de telemetria
        telemetria._ATM = atitude.angPosY;
        telemetria._RM = atitude.angVelY;
        telemetria._RV = roda.GetComponent<Rotacao>().rotacaoEmY;
        if(controleAtivado)controleAtitude(); //Usa o controle
    }

    void determinacaoAtitude()
    {
        atitude.angVelY = IMU.GetComponent<imuScript>().avaliarRotacaoIMU(); // A velocidade angular é a leitura do IMU

        // Média atitude magnetometro e LDR
        float M = Mathf.Max(atitudeMag(), atitudeLDR()),
              m = Mathf.Min(atitudeMag(), atitudeLDR());
        if (M - m < m + 360 - M) atitude.angPosY = (m + M) / 2;
        else atitude.angPosY = (m + M + 360) / 2;
        while (atitude.angPosY >= 360) atitude.angPosY -= 360;
        while (atitude.angPosY < 0) atitude.angPosY += 360;
    }

    // Determina a atitude segundo o magnetômetro
    float atitudeMag()
    {
        float att = IMU.GetComponent<imuScript>().avaliarCampoMagneticoIMU();
        telemetria._PNM =att==0?0: 360 - att; // Evita o valor 360° de aparecer
        att-=direcao.GetComponent<PropriedadesDirecao>().diferencaObjetivoCM();
        // Normalização 0~360°
        while (att >= 360) att -= 360;
        while (att < 0) att += 360;
        return att;
    }
    
    // Determina a atitude segundo os sensores LDR
    float atitudeLDR()
    {
        float f = LDR_Frente.GetComponent<LDR_Script>().avaliarLDR(),
              d = LDR_Direita.GetComponent<LDR_Script>().avaliarLDR(),
              e = LDR_Esquerda.GetComponent<LDR_Script>().avaliarLDR(),
              v = LDR_Verso.GetComponent<LDR_Script>().avaliarLDR();
        float min = Mathf.Min(f, d, e, v),att;
        if (min == f)
        {
            min = Mathf.Min(d, e, v);
            if (min == d) att = ((360 - f) + (270 + d)) / 2;
            else if (min == e) att = (f + (90 - e)) / 2;
            else att = 0; //max==v
        }
        else if (min == d)
        {
            min = Mathf.Min(f, e, v);
            if (min == f) att = ((360 - f) + (270 + d)) / 2;
            else if (min == e) att = 90;
            else att = ((v + 180) + (270 - d)) / 2; //max==v
        }
        else if (min == e)
        {
            min = Mathf.Min(f, d, v);
            if (min == d) att = 270;
            else if (min == f) att = (f + (90 - e)) / 2;
            else att = ((180 - v) + (90 + e)) / 2; //max==v
        }
        else //max==v
        {
            min = Mathf.Min(f, d, e);
            if (min == d) att = ((v + 180) + (270 - d)) / 2;
            else if (min == e) att = ((180 - v) + (90 + e)) / 2;
            else att = 180; //max==f
        }
        telemetria._SM = att==0?0:360-att;
        att -= direcao.GetComponent<PropriedadesDirecao>().diferencaObjetivoSol();
        while (att >= 360) att -= 360;
        while (att < 0) att += 360;
        return att;
    }

    float erroAnt=0;

    void controleAtitude()
    {
        float erro = atitude.angPosY; if (erro > 180) erro -= 360; // Erro sempre em -180 ~ +180°
        float rotacaoDesejada;
        rotEquilibrio = (roda.GetComponent<Rotacao>().rotacaoEmY * propRoda.Iyy + propSat.Iyy * atitude.angVelY) / propRoda.Iyy; //Conservacao H
        if (Mathf.Abs(atitude.angVelY) < rotacaoLimiteControle)
        {
            rotacaoDesejada = rotEquilibrio + Kp * erro + Kd * (erro - erroAnt) / Time.fixedDeltaTime;
        }
        else // Caso esteja muito rápido, ele só tenta zerar a rotação
        {
            rotacaoDesejada = rotEquilibrio;
        }
        motor.GetComponent<MotorScript>().rotacaoDesejada = rotacaoDesejada;
        erroAnt = erro;
    }

    // Ativa/desativa o controle
    public void botaoControleUI()
    {
        if (controleAtivado)
        {
            botaoControle.GetComponent<UnityEngine.UI.Image>().color = Color.red;
            botaoControle.GetComponentInChildren<Text>().color = Color.white;
            controleAtivado = false;
        }
        else
        {
            botaoControle.GetComponent<UnityEngine.UI.Image>().color = Color.green;
            //botaoControle.GetComponent<UnityEngine.UI.Button>().colors.normalColor = Color.green;
            botaoControle.GetComponentInChildren<Text>().color = Color.black;
            controleAtivado= true;
        }
    }

    /*
    double[][] phi, H, G, I, P_bar, P_hat, Q, K, x_bar, x_hat,R,y;

    void inicializacaoKalman()
    {
        P_hat = MatrixMath.CreateDiagonalMatrix(1,1,1,1,1);
        phi = new double[][] { new double[] { 1,Time.fixedDeltaTime,0,0,0},
            new double[]{ 0,0,-propRoda.Iyy/propSat.Iyy,0,1/propSat.Iyy},
            new double[]{ 0,0,0,0,0},
            new double[]{ 0,0,0,0,1/propRoda.Iyy},
            new double[]{ 0, propSat.Iyy,0,propRoda.Iyy,0}};
        H = new double[][] { new double[] { 1,0,0,0,0},
            new double[] { 1,0,0,0,0},
            new double[] { 0,1,0,0,0} };
        I = MatrixMath.MatrixIdentity(5);
        G = MatrixMath.MatrixIdentity(5);
        Q = MatrixMath.CreateDiagonalMatrix(1, 1, 1, 1, 1);
        x_hat = MatrixMath.MatrixCreate(5, 1);
        R = MatrixMath.CreateDiagonalMatrix(1, 1, 1, 1, 1);
    }

    void determinacaoAtitudeKalman()
    {
        x_bar = MatrixMath.MatrixProduct(phi, x_hat);
        P_bar = MatrixMath.MatrixSum(MatrixMath.IntercalateProduct(phi, P_hat), MatrixMath.IntercalateProduct(G, Q));
        K = MatrixMath.MatrixSum(R, MatrixMath.IntercalateProduct(H, P_bar));
        K = MatrixMath.MatrixProduct(MatrixMath.MatrixProduct(P_bar, MatrixMath.MatrixTranspose(H)), MatrixMath.MatrixInverse(K));
        P_hat = MatrixMath.MatrixProduct(MatrixMath.MatrixSum(I,MatrixMath.MatrixProduct(K,H),true), P_bar);
        y = MatrixMath.CreateVector(atitudeMag(), atitudeLDR(), IMU.GetComponent<imuScript>().avaliarRotacaoIMU());
        x_hat = MatrixMath.MatrixSum(y, MatrixMath.MatrixProduct(H, x_bar), true);
        x_hat = MatrixMath.MatrixSum(x_bar, MatrixMath.MatrixProduct(K, x_hat));
    }
    */
}

// Classe  para cálculo matricial
/*
public class MatrixMath
{
    // Source: https://jamesmccaffrey.wordpress.com/2015/03/06/inverting-a-matrix-using-c/
    public static double[][] MatrixCreate(int rows, int cols)
    {
        double[][] result = new double[rows][];
        for (int i = 0; i < rows; ++i)
            result[i] = new double[cols];
        return result;
    }

    // --------------------------------------------------

    public static double[][] MatrixIdentity(int n)
    {
        // return an n x n Identity matrix
        double[][] result = MatrixCreate(n, n);
        for (int i = 0; i < n; ++i)
            result[i][i] = 1.0;

        return result;
    }

    // --------------------------------------------------

    public static string MatrixAsString(double[][] matrix, int dec)
    {
        string s = "";
        for (int i = 0; i < matrix.Length; ++i)
        {
            for (int j = 0; j < matrix[i].Length; ++j)
                s += matrix[i][j].ToString("F" + dec).PadLeft(8) + " ";
            s += Environment.NewLine;
        }
        return s;
    }

    // --------------------------------------------------

    public static bool MatrixAreEqual(double[][] matrixA,
      double[][] matrixB, double epsilon)
    {
        // true if all values in matrixA == values in matrixB
        int aRows = matrixA.Length; int aCols = matrixA[0].Length;
        int bRows = matrixB.Length; int bCols = matrixB[0].Length;
        if (aRows != bRows || aCols != bCols)
            throw new Exception("Non-conformable matrices");

        for (int i = 0; i < aRows; ++i) // each row of A and B
            for (int j = 0; j < aCols; ++j) // each col of A and B
                                            //if (matrixA[i][j] != matrixB[i][j])
                if (Math.Abs(matrixA[i][j] - matrixB[i][j]) > epsilon)
                    return false;
        return true;
    }

    // --------------------------------------------------

    public static double[][] MatrixProduct(double[][] matrixA, double[][] matrixB)
    {
        int aRows = matrixA.Length; int aCols = matrixA[0].Length;
        int bRows = matrixB.Length; int bCols = matrixB[0].Length;
        if (aCols != bRows)
            throw new Exception("Non-conformable matrices in MatrixProduct");

        double[][] result = MatrixCreate(aRows, bCols);

        for (int i = 0; i < aRows; ++i) // each row of A
            for (int j = 0; j < bCols; ++j) // each col of B
                for (int k = 0; k < aCols; ++k) // could use k < bRows
                    result[i][j] += matrixA[i][k] * matrixB[k][j];

        //Parallel.For(0, aRows, i =>
        //  {
        //    for (int j = 0; j < bCols; ++j) // each col of B
        //      for (int k = 0; k < aCols; ++k) // could use k < bRows
        //        result[i][j] += matrixA[i][k] * matrixB[k][j];
        //  }
        //);

        return result;
    }

    // --------------------------------------------------

    public static double[] MatrixVectorProduct(double[][] matrix,
      double[] vector)
    {
        // result of multiplying an n x m matrix by a m x 1 
        // column vector (yielding an n x 1 column vector)
        int mRows = matrix.Length; int mCols = matrix[0].Length;
        int vRows = vector.Length;
        if (mCols != vRows)
            throw new Exception("Non-conformable matrix and vector");
        double[] result = new double[mRows];
        for (int i = 0; i < mRows; ++i)
            for (int j = 0; j < mCols; ++j)
                result[i] += matrix[i][j] * vector[j];
        return result;
    }

    // --------------------------------------------------

    public static double[][] MatrixDecompose(double[][] matrix, out int[] perm,
      out int toggle)
    {
        // Doolittle LUP decomposition with partial pivoting.
        // rerturns: result is L (with 1s on diagonal) and U;
        // perm holds row permutations; toggle is +1 or -1 (even or odd)
        int rows = matrix.Length;
        int cols = matrix[0].Length; // assume square
        if (rows != cols)
            throw new Exception("Attempt to decompose a non-square m");

        int n = rows; // convenience

        double[][] result = MatrixDuplicate(matrix);

        perm = new int[n]; // set up row permutation result
        for (int i = 0; i < n; ++i) { perm[i] = i; }

        toggle = 1; // toggle tracks row swaps.
                    // +1 -> even, -1 -> odd. used by MatrixDeterminant

        for (int j = 0; j < n - 1; ++j) // each column
        {
            double colMax = Math.Abs(result[j][j]); // find largest val in col
            int pRow = j;
            //for (int i = j + 1; i < n; ++i)
            //{
            //  if (result[i][j] > colMax)
            //  {
            //    colMax = result[i][j];
            //    pRow = i;
            //  }
            //}

            // reader Matt V needed this:
            for (int i = j + 1; i < n; ++i)
            {
                if (Math.Abs(result[i][j]) > colMax)
                {
                    colMax = Math.Abs(result[i][j]);
                    pRow = i;
                }
            }
            // Not sure if this approach is needed always, or not.

            if (pRow != j) // if largest value not on pivot, swap rows
            {
                double[] rowPtr = result[pRow];
                result[pRow] = result[j];
                result[j] = rowPtr;

                int tmp = perm[pRow]; // and swap perm info
                perm[pRow] = perm[j];
                perm[j] = tmp;

                toggle = -toggle; // adjust the row-swap toggle
            }

            // --------------------------------------------------
            // This part added later (not in original)
            // and replaces the 'return null' below.
            // if there is a 0 on the diagonal, find a good row
            // from i = j+1 down that doesn't have
            // a 0 in column j, and swap that good row with row j
            // --------------------------------------------------

            if (result[j][j] == 0.0)
            {
                // find a good row to swap
                int goodRow = -1;
                for (int row = j + 1; row < n; ++row)
                {
                    if (result[row][j] != 0.0)
                        goodRow = row;
                }

                if (goodRow == -1)
                    throw new Exception("Cannot use Doolittle's method");

                // swap rows so 0.0 no longer on diagonal
                double[] rowPtr = result[goodRow];
                result[goodRow] = result[j];
                result[j] = rowPtr;

                int tmp = perm[goodRow]; // and swap perm info
                perm[goodRow] = perm[j];
                perm[j] = tmp;

                toggle = -toggle; // adjust the row-swap toggle
            }
            // --------------------------------------------------
            // if diagonal after swap is zero . .
            //if (Math.Abs(result[j][j]) < 1.0E-20) 
            //  return null; // consider a throw

            for (int i = j + 1; i < n; ++i)
            {
                result[i][j] /= result[j][j];
                for (int k = j + 1; k < n; ++k)
                {
                    result[i][k] -= result[i][j] * result[j][k];
                }
            }


        } // main j column loop

        return result;
    } // MatrixDecompose

    // --------------------------------------------------

    public static double[][] MatrixInverse(double[][] matrix)
    {
        int n = matrix.Length;
        double[][] result = MatrixDuplicate(matrix);

        int[] perm;
        int toggle;
        double[][] lum = MatrixDecompose(matrix, out perm,
          out toggle);
        if (lum == null)
            throw new Exception("Unable to compute inverse");

        double[] b = new double[n];
        for (int i = 0; i < n; ++i)
        {
            for (int j = 0; j < n; ++j)
            {
                if (i == perm[j])
                    b[j] = 1.0;
                else
                    b[j] = 0.0;
            }

            double[] x = HelperSolve(lum, b); // 

            for (int j = 0; j < n; ++j)
                result[j][i] = x[j];
        }
        return result;
    }

    // --------------------------------------------------

    public static double MatrixDeterminant(double[][] matrix)
    {
        int[] perm;
        int toggle;
        double[][] lum = MatrixDecompose(matrix, out perm, out toggle);
        if (lum == null)
            throw new Exception("Unable to compute MatrixDeterminant");
        double result = toggle;
        for (int i = 0; i < lum.Length; ++i)
            result *= lum[i][i];
        return result;
    }

    // --------------------------------------------------

    public static double[] HelperSolve(double[][] luMatrix, double[] b)
    {
        // before calling this helper, permute b using the perm array
        // from MatrixDecompose that generated luMatrix
        int n = luMatrix.Length;
        double[] x = new double[n];
        b.CopyTo(x, 0);

        for (int i = 1; i < n; ++i)
        {
            double sum = x[i];
            for (int j = 0; j < i; ++j)
                sum -= luMatrix[i][j] * x[j];
            x[i] = sum;
        }

        x[n - 1] /= luMatrix[n - 1][n - 1];
        for (int i = n - 2; i >= 0; --i)
      {
            double sum = x[i];
            for (int j = i + 1; j < n; ++j)
                sum -= luMatrix[i][j] * x[j];
            x[i] = sum / luMatrix[i][i];
        }

        return x;
    }

    // --------------------------------------------------

    public static double[] SystemSolve(double[][] A, double[] b)
    {
        // Solve Ax = b
        int n = A.Length;

        // 1. decompose A
        int[] perm;
        int toggle;
        double[][] luMatrix = MatrixDecompose(A, out perm,
          out toggle);
        if (luMatrix == null)
            return null;

        // 2. permute b according to perm[] into bp
        double[] bp = new double[b.Length];
        for (int i = 0; i < n; ++i)
            bp[i] = b[perm[i]];

        // 3. call helper
        double[] x = HelperSolve(luMatrix, bp);
        return x;
    } // SystemSolve

    // --------------------------------------------------

    public static double[][] MatrixDuplicate(double[][] matrix)
    {
        // allocates/creates a duplicate of a matrix.
        double[][] result = MatrixCreate(matrix.Length, matrix[0].Length);
        for (int i = 0; i < matrix.Length; ++i) // copy the values
            for (int j = 0; j < matrix[i].Length; ++j)
                result[i][j] = matrix[i][j];
        return result;
    }

    // --------------------------------------------------

    // Daqui pra baixo, eu (Augusto) quem escrevi

    public static double[][] MatrixSum(double[][] a, double[][] b, bool subtract=false)
    {
        if (a.Length != b.Length || a[0].Length != b[0].Length) throw new Exception("Lengths don't match");
        double[][] aux = MatrixCreate(a.Length, a[0].Length);
        for (int i = 0; i < a.Length; i++)
        {
            for (int j = 0; j < a[0].Length; j++)
            {
                if(!subtract)aux[i][j] = a[i][j] + b[i][j];
                else aux[i][j] = a[i][j] - b[i][j];
            }
        }
        return aux;
    }

    public static double[][] MatrixNumberProduct(double number,double[][] matrix)
    {
        double[][] aux = MatrixCreate(matrix.Length, matrix[0].Length);
        for (int i = 0; i < matrix.Length; i++)
        {
            for (int j = 0; j < matrix[0].Length; j++)
            {
                aux[i][j] = number*matrix[i][j];
            }
        }
        return aux;
    }

    public static double[][] MatrixTranspose(double[][] matrix)
    {
        double[][] aux = MatrixCreate(matrix.Length, matrix[0].Length);
        for (int i = 0; i < matrix.Length; i++)
        {
            for (int j = 0; j < matrix[0].Length; j++)
            {
                aux[i][j] = matrix[j][i];
            }
        }
        return aux;
    }

    public static double[][] CreateDiagonalMatrix(params double[] diagonal)
    {
        double[][] aux = MatrixCreate(diagonal.Length, diagonal.Length);
        for (int i = 0; i < diagonal.Length; i++)
        {
             aux[i][i] = diagonal[i];
        }
        return aux;
    }

    public static double[][] CreateVector(params double[] values)
    {
        double[][] aux = MatrixCreate(values.Length, 1);
        for (int i = 0; i < values.Length; i++)
        {
            aux[i][0] = values[i];
        }
        return aux;
    }

    public static double[][] IntercalateProduct(double[][] a, double[][] c)
    {
        return MatrixProduct(MatrixProduct(a, c), MatrixTranspose(a));
    }
}

    */