﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Script que retorna propriedades relacionadas à atitude-objetivo
public class PropriedadesDirecao : MonoBehaviour
{
    [SerializeField]
    GameObject campoMagnetico;
    [SerializeField]
    GameObject sol;

    public float diferencaObjetivoCM() // Diferença atitude-objetivo e campo magnético
    {
        return transform.eulerAngles.y - campoMagnetico.transform.eulerAngles.y;
    }
    public float diferencaObjetivoSol() // Diferença atitude-objetivo e vetor solar
    {
        return transform.eulerAngles.y - sol.transform.eulerAngles.y;
    }
}
